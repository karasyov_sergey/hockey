package ru.vanvan.hockey;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.sql.SQLException;

public class Application extends AppCompatActivity implements IPrefKey, IAdministrator {
    private NavController navController;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},
                PackageManager.PERMISSION_GRANTED);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        ApplicationModel.createInstance(this);
        prefs = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        try {
            AttackerDB.setDriver();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            AppToast.showToast(this, R.string.driver_error_msg);
            return;
        }
        toLogIn();
    }

    private void toLogIn() {
        try {
            if (!prefs.contains(EMAIL_KEY)) {
                return;
            }
            final String email = prefs.getString(EMAIL_KEY, "");
            if (email == null) {
                return;
            }
            if (email.equals(ADM_EMAIL)) {
                createSystemAcc(email, AccType.ADMINISTRATOR);
                return;
            }
            createSystemAcc(email, AccType.USER);
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(this, R.string.connection_error_msg);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.main_cabinet_item) {
            return onMainCabinetItemClick();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean onMainCabinetItemClick() {
        if (SystemAccount.getInstance() == null) {
            navigateTo(R.id.nav_authorization_fragment);
            return true;
        }
        if (SystemAccount.getInstance().getType().equals(AccType.ADMINISTRATOR)) {
            navigateTo(R.id.nav_administrator_cabinet_fragment);
            return true;
        }
        navigateTo(R.id.nav_main_cabinet_fragment);
        return true;
    }

    public void navigateTo(int navId) {
        navController.navigate(navId);
    }

    public void createSystemAcc(@NonNull final String email, AccType type) throws SQLException {
        SystemAccount.buildAcc(email, type);
    }

    public void toLogOut() {
        prefs.edit().clear().apply();
        SystemAccount.deleteAcc();
        AppToast.showToast(this, R.string.log_out_msg);
        navigateTo(R.id.nav_team_fragment);
    }
}