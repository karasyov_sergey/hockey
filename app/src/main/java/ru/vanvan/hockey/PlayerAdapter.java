package ru.vanvan.hockey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerView> {
    private List<Player> playerList;

    public PlayerAdapter(List<Player> playerList) {
        this.playerList = playerList;
    }

    @NonNull
    @Override
    public PlayerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_element, parent, false);
        return new PlayerView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerView holder, int position) {
        holder.setPlayer(playerList.get(position));
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public static class PlayerView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView nameTV;
        private final TextView surnameTV;
        private final TextView middleNameTV;
        private Player player;
        private Context context;

        public PlayerView(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            nameTV = itemView.findViewById(R.id.request_name_tv);
            surnameTV = itemView.findViewById(R.id.request_surname_tv);
            middleNameTV = itemView.findViewById(R.id.request_middle_name_tv);
            context = itemView.getContext();
        }

        public void setPlayer(Player player) {
            this.player = player;
            bindToUiData();
        }

        private void bindToUiData() {
            final String name = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_name_tv), player.getName());
            final String surname = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_surname_tv), player.getSurname());
            final String middleName = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_middle_name_tv), player.getMiddleName());
            nameTV.setText(name);
            surnameTV.setText(surname);
            middleNameTV.setText(middleName);
        }

        @Override
        public void onClick(View view) {
            ApplicationModel model = ApplicationModel.getInstance();
            model.setCurrentPlayer(player);
            model.getApp().navigateTo(R.id.nav_player_fragment);
        }
    }
}
