package ru.vanvan.hockey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.TeamView> {

    private List<Match> matchList;

    public MatchAdapter(List<Match> matchList) {
        this.matchList = matchList;
    }

    @NonNull
    @Override
    public TeamView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_element, parent, false);
        return new TeamView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamView holder, int position) {
        holder.setHeader(matchList.get(position));
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

    public static class TeamView extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Match match;
        private TextView headerTV;
        private TextView matchDateTV;
        private Context context;

        public TeamView(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            itemView.setOnClickListener(this);
            headerTV = itemView.findViewById(R.id.match_header_tv);
            matchDateTV = itemView.findViewById(R.id.match_date_tv);
        }

        public void setHeader(Match match) {
            this.match = match;
            bindMatchToView();
        }

        private void bindMatchToView() {
            final String rowHeader = context.getString(R.string.match_header_tv);
            final String header = String.format(Locale.ENGLISH, rowHeader, match.getTeamLeft(), match.getTeamRight());
            headerTV.setText(header);
            matchDateTV.setText(match.getDate().toString());
        }

        @Override
        public void onClick(View view) {
            ApplicationModel model = ApplicationModel.getInstance();
            model.setCurrentMatch(match);
            model.getApp().navigateTo(R.id.nav_player_list_fragment);
        }
    }
}
