package ru.vanvan.hockey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.SQLException;
import java.util.List;

public class MatchFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_match, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView matchRV = view.findViewById(R.id.match_rv);
        matchRV.setLayoutManager(new LinearLayoutManager(view.getContext()));
        try {
            List<Match> matchList = AttackerDB.getMatches();
            matchRV.setAdapter(new MatchAdapter(matchList));
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(view.getContext(), R.string.connection_error_msg);
        }
    }
}