package ru.vanvan.hockey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainCabinetFragment extends Fragment {
    private ApplicationModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_main_cabinet, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = ApplicationModel.getInstance();
        SystemAccount account = SystemAccount.getInstance();
        TextView userEmailTV = view.findViewById(R.id.user_email_tv);
        userEmailTV.setText(account.getEmail());
        RecyclerView requestRV = view.findViewById(R.id.request_rv);
        requestRV.setLayoutManager(new LinearLayoutManager(view.getContext()));
        requestRV.setAdapter(new RequestAdapter(account.getRequestList(), account.getType()));
        Button logOutBtn = view.findViewById(R.id.log_out_btn);
        logOutBtn.setOnClickListener(this::toLogOutBtnClick);
        Button createRequestClick = view.findViewById(R.id.create_request_btn);
        createRequestClick.setOnClickListener(this::onCreateRequestBtnClick);

    }

    private void toLogOutBtnClick(View view) {
        model.getApp().toLogOut();
    }

    private void onCreateRequestBtnClick(View view) {
        model.getApp().navigateTo(R.id.nav_request_fragment);
    }
}
