package ru.vanvan.hockey;

public class ApplicationModel {

    public static ApplicationModel instance;
    private final Application app;
    private Match currentMatch;
    private Player currentPlayer;

    public static void createInstance(final Application app) {
        if (instance != null) {
            return;
        }
        instance = new ApplicationModel(app);
    }

    public static ApplicationModel getInstance() {
        return instance;
    }

    private ApplicationModel(final Application app) {
        this.app = app;
    }

    public Application getApp() {
        return app;
    }

    public void setCurrentMatch(Match currentMatch) {
        this.currentMatch = currentMatch;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Match getCurrentMatch() {
        return currentMatch;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
}
