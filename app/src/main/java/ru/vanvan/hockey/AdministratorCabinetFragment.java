package ru.vanvan.hockey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AdministratorCabinetFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_administrator_cabinet, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SystemAccount account = SystemAccount.getInstance();
        RecyclerView requestRV = view.findViewById(R.id.request_rv);
        requestRV.setLayoutManager(new LinearLayoutManager(view.getContext()));
        requestRV.setAdapter(new RequestAdapter(account.getRequestList(), account.getType()));
        Button logOutBtn = view.findViewById(R.id.log_out_btn);
        logOutBtn.setOnClickListener(this::toLogOutBtnClick);
    }

    private void toLogOutBtnClick(View view) {
        ApplicationModel.getInstance().getApp().toLogOut();
    }
}
