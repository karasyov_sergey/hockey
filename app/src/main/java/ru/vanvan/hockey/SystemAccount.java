package ru.vanvan.hockey;

import java.sql.SQLException;
import java.util.List;

public class SystemAccount {
    private static SystemAccount instance;
    private final String email;
    private final AccType type;
    private List<Request> requestList;

    public static void buildAcc(String email, AccType type) throws SQLException {
        if (instance != null) {
            return;
        }
        instance = new SystemAccount(email, type);
    }

    public static SystemAccount getInstance() {
        return instance;
    }

    private SystemAccount(String email, AccType type) throws SQLException {
        this.email = email;
        this.type = type;
        switch (type) {
            case USER:
                setUserRequestList();
                break;
            case ADMINISTRATOR:
                setAdmRequestList();
                break;
            default:
        }
    }

    private void setAdmRequestList() throws SQLException {
        requestList = AttackerDB.getAllRequestList();
    }

    public void setUserRequestList() throws SQLException {
        requestList = AttackerDB.getRequestList(email);
    }

    public static void deleteAcc() {
        instance = null;
    }

    public String getEmail() {
        return email;
    }

    public List<Request> getRequestList() {
        return requestList;
    }

    public void addRequest(Request request) {
        requestList.add(request);
    }

    public AccType getType() {
        return type;
    }

    public void removeRequest(Request request) {
        requestList.remove(request);
    }
}
