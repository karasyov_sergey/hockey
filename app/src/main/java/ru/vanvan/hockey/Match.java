package ru.vanvan.hockey;

import java.sql.Date;

public class Match {
    private final int id;
    private final Date date;
    private final int countLeft;
    private final int countRight;
    private final String teamLeft;
    private final String teamRight;

    public Match(int id, Date date, int countLeft, int countRight, String teamLeft, String teamRight) {
        this.id = id;
        this.date = date;
        this.countLeft = countLeft;
        this.countRight = countRight;
        this.teamLeft = teamLeft;
        this.teamRight = teamRight;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public int getCountLeft() {
        return countLeft;
    }

    public int getCountRight() {
        return countRight;
    }

    public String getTeamLeft() {
        return teamLeft;
    }

    public String getTeamRight() {
        return teamRight;
    }
}
