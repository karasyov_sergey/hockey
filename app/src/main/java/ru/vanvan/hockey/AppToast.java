package ru.vanvan.hockey;

import android.content.Context;
import android.widget.Toast;

public class AppToast {

    private AppToast() {
    }

    public static void showToast(Context context, int stringId) {
        Toast.makeText(context, context.getString(stringId), Toast.LENGTH_LONG).show();
    }
}
