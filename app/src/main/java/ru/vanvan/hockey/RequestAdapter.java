package ru.vanvan.hockey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestView> {
    private final List<Request> requestList;
    private final AccType type;

    public RequestAdapter(List<Request> requestList, AccType type) {
        this.requestList = requestList;
        this.type = type;
    }

    @NonNull
    @Override
    public RequestView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_element, parent, false);
        return new RequestView(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestView holder, int position) {
        holder.setRequest(requestList.get(position));
    }

    public AccType getPermissionType() {
        return type;
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    public static class RequestView extends RecyclerView.ViewHolder {
        private final TextView nameTV;
        private final TextView surnameTV;
        private final TextView middleNameTV;
        private final TextView teamTV;
        private final TextView emailTV;
        private final Context context;
        private Request request;
        private RequestAdapter adapter;

        public RequestView(@NonNull View itemView, RequestAdapter adapter) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.request_name_tv);
            surnameTV = itemView.findViewById(R.id.request_surname_tv);
            middleNameTV = itemView.findViewById(R.id.request_middle_name_tv);
            teamTV = itemView.findViewById(R.id.request_team_tv);
            emailTV = itemView.findViewById(R.id.request_email_tv);
            context = itemView.getContext();
            this.adapter = adapter;
            if (adapter.getPermissionType().equals(AccType.ADMINISTRATOR)) {
                Button approveBtn = itemView.findViewById(R.id.approve_btn);
                approveBtn.setVisibility(View.VISIBLE);
                approveBtn.setOnClickListener(this::onApproveBtnClick);
                Button cancelBtn = itemView.findViewById(R.id.cancel_btn);
                cancelBtn.setVisibility(View.VISIBLE);
                cancelBtn.setOnClickListener(this::onCancelBtnClick);
            }
        }

        private void onCancelBtnClick(View view) {
            try {
                AppToast.showToast(view.getContext(), R.string.cancel_request_msg);
                adapter.requestList.remove(request);
                adapter.notifyDataSetChanged();
                AttackerDB.cancelRequest(request.getId());
            } catch (SQLException e) {
                e.printStackTrace();
                AppToast.showToast(view.getContext(), R.string.connection_error_msg);
            }
        }

        private void onApproveBtnClick(View view) {
            try {
                if (AttackerDB.doRequest(request)) {
                    AppToast.showToast(view.getContext(), R.string.request_was_approve_msg);
                    adapter.requestList.remove(request);
                    adapter.notifyDataSetChanged();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                AppToast.showToast(view.getContext(), R.string.connection_error_msg);
            }
        }

        public void setRequest(Request request) {
            this.request = request;
            bindToUi();
        }

        private void bindToUi() {
            final String name = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_name_tv), request.getName());
            final String surname = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_surname_tv), request.getSurname());
            final String middleName = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_middle_name_tv), request.getMiddleName());
            final String team = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_team_tv), request.getTeam().getName());
            final String email = String.format(Locale.ENGLISH,
                    context.getString(R.string.player_email_tv), request.getEmail());

            nameTV.setText(name);
            surnameTV.setText(surname);
            middleNameTV.setText(middleName);
            teamTV.setText(team);
            emailTV.setText(email);
        }
    }
}
