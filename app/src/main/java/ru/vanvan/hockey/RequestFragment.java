package ru.vanvan.hockey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RequestFragment extends Fragment {
    private TextView nameET;
    private TextView surnameET;
    private TextView middleNameET;
    private Spinner teamSpinner;
    private SystemAccount account;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_request, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nameET = view.findViewById(R.id.name_et);
        surnameET = view.findViewById(R.id.surname_et);
        middleNameET = view.findViewById(R.id.middle_name_et);
        teamSpinner = view.findViewById(R.id.team_spinner);
        Button createRequestBtn = view.findViewById(R.id.create_request_btn);
        createRequestBtn.setOnClickListener(this::onCreateRequestBtnClick);
        account = SystemAccount.getInstance();
        try {
            List<Team> teamList = AttackerDB.getTeamList();
            List<String> teamNameList = getTeamNameList(teamList);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                    android.R.layout.simple_spinner_item, teamNameList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            teamSpinner.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(view.getContext(), R.string.connection_error_msg);
        }
    }

    private List<String> getTeamNameList(List<Team> teamList) {
        List<String> teamNameList = new ArrayList<>();
        for (Team aTeam : teamList) {
            teamNameList.add(aTeam.getName());
        }
        return teamNameList;
    }

    private void onCreateRequestBtnClick(View view) {
        final String name = nameET.getText().toString();
        final String surname = surnameET.getText().toString();
        final String middleName = middleNameET.getText().toString();
        Team team = new Team(teamSpinner.getSelectedItem().toString());
        if (containsRequestTo(team)) {
            AppToast.showToast(view.getContext(), R.string.you_have_request_to_team_msg);
            return;
        }
        if (!isValidData(name, surname, middleName)) {
            return;
        }
        try {
            AttackerDB.addRequest(name, surname, middleName, account.getEmail(), team);
            account.setUserRequestList();
            ApplicationModel.getInstance().getApp().navigateTo(R.id.nav_main_cabinet_fragment);
            AppToast.showToast(view.getContext(), R.string.request_is_successfully_msg);
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(view.getContext(), R.string.connection_error_msg);
        }
    }

    private boolean containsRequestTo(Team team) {
        List<Request> requestList = account.getRequestList();
        for (Request aRequest : requestList) {
            if (aRequest.getTeam().getName().equals(team.getName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isValidData(@NonNull final String name,
                                @NonNull final String surname,
                                @NonNull final String middleName) {
        return isValidName(name) && isValidSurname(surname) && isValidMiddleName(middleName);
    }

    private boolean isValidMiddleName(String middleName) {
        if (middleName.trim().isEmpty()) {
            middleNameET.setError(getString(R.string.input_your_middle_name_msg));
            return false;
        }
        return true;
    }

    private boolean isValidSurname(String surname) {
        if (surname.trim().isEmpty()) {
            surnameET.setError(getString(R.string.input_your_surname_msg));
        }
        return true;
    }

    private boolean isValidName(String name) {
        if (name.trim().isEmpty()) {
            nameET.setError(getString(R.string.input_your_name_msg));
        }
        return true;
    }
}
