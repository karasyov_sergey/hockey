package ru.vanvan.hockey;

public class Request {
    private final String name;
    private final String surname;
    private final String middleName;
    private final Team team;
    private final String email;
    private final int id;

    public Request(final int id, final String name, final String surname,
                   final String middleName, final String email, final Team team) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.email = email;
        this.team = team;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Team getTeam() {
        return team;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }
}
