package ru.vanvan.hockey;

public interface IAttributes {
    String ID = "id";
    String NAME = "name";
    String SURNAME = "surname";
    String MIDDLE_NAME = "middle_name";
    String TEAM_NAME = "team_name";
    String ENTERED_DATE = "entered_date";
    String PENALTY_TIME = "penalty_time";
    String GENERAL_COUNT = "general_count";
    String SUPPLY_COUNT = "supply_count";
    String GAME_QUANTITY = "game_quantity";
    String ACC_COUNT = "acc_count";
    String EMAIL = "email";
    String TEAM = "team";
    String PLAYER_ID = "player_id";
    String MATCH_ID = "match_id";
    String COUNT_LEFT = "count_left";
    String COUNT_RIGHT = "count_right";
    String TEAM_LEFT = "team_left";
    String TEAM_RIGHT = "team_right";
    String DATE = "date";
}
