package ru.vanvan.hockey;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.SQLException;

public class AuthorizationFragment extends Fragment implements IPrefKey, IAdministrator {
    private EditText emailET;
    private EditText passwordET;
    private ApplicationModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_authorization, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailET = view.findViewById(R.id.email_et);
        passwordET = view.findViewById(R.id.password_et);
        model = ApplicationModel.getInstance();
        Button authorizationBtn = view.findViewById(R.id.authorization_btn);
        authorizationBtn.setOnClickListener(this::onAuthorizationBtnClick);
        Button toRegistrationBtn = view.findViewById(R.id.to_registration_btn);
        toRegistrationBtn.setOnClickListener(this::onToRegistrationBtnClick);
    }

    private void onToRegistrationBtnClick(View view) {
        model.getApp().navigateTo(R.id.nav_registration_fragment);
    }

    private void onAuthorizationBtnClick(View view) {
        final String email = emailET.getText().toString();
        final String password = passwordET.getText().toString();
        try {
            if (isAdministrator(email, password)) {
                toLogIn(view.getContext(), email, AccType.ADMINISTRATOR);
                return;
            }
            if (AttackerDB.containsAcc(email, password)) {
                toLogIn(view.getContext(), email, AccType.USER);
                return;
            }
            emailET.setError(getString(R.string.invalid_log_in_msg));
            passwordET.setError(getString(R.string.invalid_log_in_msg));
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(view.getContext(), R.string.connection_error_msg);
        }
    }

    private boolean isAdministrator(String email, String password) {
        return email.equals(ADM_EMAIL) && password.equals(ADM_PASSWORD);
    }

    private void toLogIn(Context context, String email, AccType type) throws SQLException {
        toCacheAcc(context, email);
        AppToast.showToast(context, R.string.final_authorization_msg);
        Application app = model.getApp();
        app.createSystemAcc(email, type);
        app.navigateTo(R.id.nav_team_fragment);
    }

    private void toCacheAcc(Context context, String email) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString(EMAIL_KEY, email).apply();
    }
}
