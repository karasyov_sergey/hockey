package ru.vanvan.hockey;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.SQLException;

public class RegistrationFragment extends Fragment implements View.OnClickListener {
    private EditText emailET;
    private EditText passwordET;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_registration, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailET = view.findViewById(R.id.email_et);
        passwordET = view.findViewById(R.id.password_et);
        Button registrationBtn = view.findViewById(R.id.registration_btn);
        registrationBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        final String email = emailET.getText().toString();
        final String password = passwordET.getText().toString();
        try {
            if (AttackerDB.containsAcc(email, password)) {
                emailET.setError(getString(R.string.account_is_exists_msg));
                return;
            }
            if (!isValidAccData(email, password)) {
                return;
            }
            AttackerDB.addAccount(email, password);
            ApplicationModel.getInstance().getApp().navigateTo(R.id.nav_authorization_fragment);
            AppToast.showToast(view.getContext(), R.string.final_registration_msg);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidAccData(@NonNull final String email, @NonNull final String password) {
        return isValidEmail(email) && isValidPassword(password);
    }

    private boolean isValidPassword(String password) {
        if (password.trim().isEmpty()) {
            passwordET.setError(getString(R.string.input_your_password_msg));
            return false;
        }
        if (password.length() < 6) {
            passwordET.setError(getString(R.string.password_too_short_msg));
            return false;
        }
        return true;
    }

    private boolean isValidEmail(@NonNull final String email) {
        if (!email.contains("@")) {
            emailET.setError(getString(R.string.invalid_email_format_msg));
            return false;
        }
        if (email.trim().isEmpty()) {
            emailET.setError(getString(R.string.input_your_email_msg));
            return false;
        }
        return true;
    }
}
