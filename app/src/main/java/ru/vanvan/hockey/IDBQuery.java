package ru.vanvan.hockey;

public interface IDBQuery {

    String GET_PLAYERS_QUERY = "exec get_players '%s';";
    String GET_ADDITIONAL_PLAYER_DATA_QUERY = "exec get_additional_player_data %d;";
    String ADD_ACC_QUERY = "exec add_acc '%s', '%s';";
    String CONTAINS_ACC_QUERY = "exec contains_acc '%s', '%s';";
    String ADD_REQUEST_QUERY = "exec add_request '%s', '%s', '%s', '%s', '%s';";
    String DO_REQUEST_QUERY = "exec do_request %d;";
    String GET_TEAMS_QUERY = "exec get_teams;";
    String GET_ALL_REQUESTS_QUERY = "exec get_all_requests;";
    String GET_USER_REQUESTS = "exec get_user_requests '%s';";
    String CANCEL_REQUEST_QUERY = "exec cancel_request %d;";
    String GET_MATCHES_QUERY = "exec get_matches;";
    String GET_PLAYER_BY_ID_QUERY = "exec get_player_by_id %d;";
}
