package ru.vanvan.hockey;

import androidx.annotation.NonNull;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class AttackerDB implements IDBConnection, IDBQuery, IAttributes {


    private AttackerDB() {

    }

    public static List<Match> getMatches() throws SQLException {
        final List<Match> matchList = new ArrayList<>();
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final ResultSet set = statement.executeQuery(GET_MATCHES_QUERY);
            while (set.next()) {
                final int id = set.getInt(ID);
                final Date date = set.getDate(DATE);
                final String teamLeft = set.getString(TEAM_LEFT);
                final String teamRight = set.getString(TEAM_RIGHT);
                final int countLeft = set.getInt(COUNT_LEFT);
                final int countRight = set.getInt(COUNT_RIGHT);
                final Match match = new Match(id, date, countLeft, countRight, teamLeft, teamRight);
                matchList.add(match);
            }
        }
        return matchList;
    }

    public static Player getPlayerById(final int id) throws SQLException {
        Player player;
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, GET_PLAYER_BY_ID_QUERY, id);
            final ResultSet set = statement.executeQuery(finallyQuery);
            set.next();
            player = new Player(id, set.getString(NAME), set.getString(SURNAME), set.getString(MIDDLE_NAME), set.getString(TEAM));
        }
        return player;
    }

    public static void cancelRequest(final int id) throws SQLException {
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, CANCEL_REQUEST_QUERY, id);
            statement.executeUpdate(finallyQuery);
        }
    }

    public static List<Request> getRequestList(@NonNull final String email) throws SQLException {
        List<Request> requestList = new ArrayList<>();
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, GET_USER_REQUESTS, email);
            ResultSet set = statement.executeQuery(finallyQuery);
            while (set.next()) {
                requestList.add(getRequest(set));
            }
        }
        return requestList;
    }

    private static Request getRequest(ResultSet set) throws SQLException {
        final int id = set.getInt(ID);
        final String name = set.getString(NAME);
        final String surname = set.getString(SURNAME);
        final String middleName = set.getString(MIDDLE_NAME);
        final String email = set.getString(EMAIL);
        final Team team = new Team(set.getString(TEAM));
        return new Request(id, name, surname, middleName, email, team);
    }

    public static List<Request> getAllRequestList() throws SQLException {
        List<Request> requestList = new ArrayList<>();
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            ResultSet set = statement.executeQuery(GET_ALL_REQUESTS_QUERY);
            while (set.next()) {
                requestList.add(getRequest(set));
            }
        }
        return requestList;
    }

    public static boolean doRequest(@NonNull final Request request) throws SQLException {
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, DO_REQUEST_QUERY, request.getId());
            statement.executeUpdate(finallyQuery);
        }
        return true;
    }

    public static void addRequest(@NonNull final String name, @NonNull final String surname,
                                  @NonNull final String middleName, @NonNull final String email,
                                  @NonNull final Team team) throws SQLException {
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, ADD_REQUEST_QUERY, name,
                    surname, middleName, email, team.getName());
            statement.executeUpdate(finallyQuery);
        }
    }

    public static boolean containsAcc(@NonNull final String email, @NonNull final String password) throws SQLException {
        int count;
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, CONTAINS_ACC_QUERY, email, password);
            final ResultSet set = statement.executeQuery(finallyQuery);
            set.next();
            count = set.getInt(ACC_COUNT);
        }
        return count > 0;
    }

    public static void addAccount(@NonNull final String email, @NonNull final String password) throws SQLException {
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, ADD_ACC_QUERY, email, password);
            statement.executeUpdate(finallyQuery);
        }
    }

    public static Player getAllPlayerData(@NonNull Player player) throws SQLException {
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, GET_ADDITIONAL_PLAYER_DATA_QUERY, player.getId());
            final ResultSet set = statement.executeQuery(finallyQuery);
            set.next();
            final Date date = set.getDate(ENTERED_DATE);
            final Time penaltyTime = set.getTime(PENALTY_TIME);
            final int generalCount = set.getInt(GENERAL_COUNT);
            final int supplyCount = set.getInt(SUPPLY_COUNT);
            final int gameQuantity = set.getInt(GAME_QUANTITY);
            player.setEnteredDate(date);
            player.setPenaltyTime(penaltyTime);
            player.setGeneralCount(generalCount);
            player.setSupplyCount(supplyCount);
            player.setGameQuantity(gameQuantity);
        }
        return player;
    }

    public static List<Player> getPlayerList(@NonNull final Team target) throws SQLException {
        final List<Player> playerList = new ArrayList<>();
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final String finallyQuery = String.format(Locale.ENGLISH, GET_PLAYERS_QUERY, target.getName());
            final ResultSet set = statement.executeQuery(finallyQuery);
            while (set.next()) {
                final int id = set.getInt(ID);
                final String name = set.getString(NAME);
                final String surname = set.getString(SURNAME);
                final String middleName = set.getString(MIDDLE_NAME);
                final String team = set.getString(TEAM_NAME);
                final Player player = new Player(id, name, surname, middleName, team);
                playerList.add(player);
            }
        }
        return playerList;
    }

    public static List<Team> getTeamList() throws SQLException {
        final List<Team> teamList = new ArrayList<>();
        try (final Connection thread = getConnection()) {
            final Statement statement = thread.createStatement();
            final ResultSet set = statement.executeQuery(GET_TEAMS_QUERY);
            while (set.next()) {
                final Team team = new Team(set.getString(NAME));
                teamList.add(team);
            }
        }
        return teamList;
    }

    public static void setDriver() throws ClassNotFoundException {
        Class.forName(DRIVER);
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }
}
