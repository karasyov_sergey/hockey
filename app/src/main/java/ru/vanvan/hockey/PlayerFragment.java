package ru.vanvan.hockey;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.sql.SQLException;
import java.util.Locale;

public class PlayerFragment extends Fragment {
    private TextView nameTV;
    private TextView surnameTV;
    private TextView middleNameTV;
    private TextView teamNameTV;
    private TextView enteringDateTV;
    private TextView penaltyTimeTV;
    private TextView generalCountTV;
    private TextView supplyCountTV;
    private TextView gameQuantityTV;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_player, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findUi(view);
        try {
            Player player = AttackerDB.getAllPlayerData(ApplicationModel.getInstance().getCurrentPlayer());
            bindToUi(view.getContext(), player);
        } catch (SQLException e) {
            e.printStackTrace();
            AppToast.showToast(view.getContext(), R.string.connection_error_msg);
        }
    }

    private void bindToUi(Context context, Player player) {
        final String name = String.format(Locale.ENGLISH,
                context.getString(R.string.player_name_tv), player.getName());
        final String surname = String.format(Locale.ENGLISH,
                context.getString(R.string.player_surname_tv), player.getSurname());
        final String middleName = String.format(Locale.ENGLISH,
                context.getString(R.string.player_middle_name_tv), player.getMiddleName());
        final String teamName = String.format(Locale.ENGLISH,
                context.getString(R.string.player_team_tv), player.getTeam());
        final String enteringDate = String.format(Locale.ENGLISH,
                context.getString(R.string.player_date_entered_tv), player.getEnteredDate().toString());
        final String penaltyTime = String.format(Locale.ENGLISH,
                context.getString(R.string.player_penalty_time_tv), player.getPenaltyTime().toString());
        final String generalCount = String.format(Locale.ENGLISH,
                context.getString(R.string.player_general_count_tv), player.getGeneralCount());
        final String supplyCount = String.format(Locale.ENGLISH,
                context.getString(R.string.player_supply_count_tv), player.getSupplyCount());
        final String gameQuantity = String.format(Locale.ENGLISH,
                context.getString(R.string.player_game_quantity_tv), player.getGameQuantity());
        nameTV.setText(name);
        surnameTV.setText(surname);
        middleNameTV.setText(middleName);
        teamNameTV.setText(teamName);
        enteringDateTV.setText(enteringDate);
        penaltyTimeTV.setText(penaltyTime);
        generalCountTV.setText(generalCount);
        supplyCountTV.setText(supplyCount);
        gameQuantityTV.setText(gameQuantity);
    }

    private void findUi(View view) {
        nameTV = view.findViewById(R.id.request_name_tv);
        surnameTV = view.findViewById(R.id.request_surname_tv);
        middleNameTV = view.findViewById(R.id.request_middle_name_tv);
        teamNameTV = view.findViewById(R.id.player_team_tv);
        enteringDateTV = view.findViewById(R.id.player_entered_date_tv);
        penaltyTimeTV = view.findViewById(R.id.player_penalty_time_tv);
        generalCountTV = view.findViewById(R.id.player_general_count_tv);
        supplyCountTV = view.findViewById(R.id.player_supply_count_tv);
        gameQuantityTV = view.findViewById(R.id.player_game_quantity_tv);
    }
}
