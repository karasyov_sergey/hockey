package ru.vanvan.hockey;

import java.sql.Date;
import java.sql.Time;

public class Player {
    private final int id;
    private final String name;
    private final String surname;
    private final String middleName;
    private final String team;
    private Date enteredDate;
    private Time penaltyTime;
    private int generalCount;
    private int supplyCount;
    private int gameQuantity;

    public Player(final int id, final String name, final String surname,
                  final String middleName, final String team) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.team = team;
        generalCount = 0;
        supplyCount = 0;
        gameQuantity = 0;
    }

    public void setEnteredDate(Date enteredDate) {
        if (this.enteredDate != null) {
            return;
        }
        this.enteredDate = enteredDate;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getTeam() {
        return team;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public Time getPenaltyTime() {
        return penaltyTime;
    }

    public void setPenaltyTime(Time penaltyTime) {
        this.penaltyTime = penaltyTime;
    }

    public int getGeneralCount() {
        return generalCount;
    }

    public void setGeneralCount(int generalCount) {
        this.generalCount = generalCount;
    }

    public int getSupplyCount() {
        return supplyCount;
    }

    public void setSupplyCount(int supplyCount) {
        this.supplyCount = supplyCount;
    }

    public int getGameQuantity() {
        return gameQuantity;
    }

    public void setGameQuantity(int gameQuantity) {
        this.gameQuantity = gameQuantity;
    }
}
