package ru.vanvan.hockey;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void teamListTest() {
        try {
            assertFalse(AttackerDB.getTeamList().isEmpty());
        } catch (SQLException e) {
            fail();
        }
    }

    @Test
    public void playerListTest() {
        try {
            assertFalse(AttackerDB.getPlayerList(new Team("Спартак")).isEmpty());
        } catch (SQLException e) {
            fail();
        }
    }

    @Test
    public void allRequestListTest() {
        try {
            assertTrue(AttackerDB.getAllRequestList().isEmpty());
        } catch (SQLException e) {
            fail();
        }
    }

    @Test
    public void userRequestListTest() {
        try {
            assertTrue(AttackerDB.getRequestList("test@gmail.com").isEmpty());
        } catch (SQLException e) {
            fail();
        }
    }
}